from django.contrib import admin
from locations.models import *

class CityAdmin(admin.ModelAdmin):
    list_display = ('name','get_region')
    search_fields=('name',)

    def get_region(self,obj):
        return obj.region_set.all().count()

class RegionAdmin(admin.ModelAdmin):
    search_fields=('name',)

class PincodeAdmin(admin.ModelAdmin):
    search_fields=('pincode',)

admin.site.register(State)
admin.site.register(City,CityAdmin)
admin.site.register(Region,RegionAdmin)
admin.site.register(Pincode,PincodeAdmin)