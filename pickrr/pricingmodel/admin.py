from django.contrib import admin
from pricingmodel.models import *

class PickrrZoneAdmin(admin.ModelAdmin):
    list_display=('zone_name','city_count')

    def city_count(self,obj):
        return obj.city_set.all().count()

admin.site.register(PickrrZone,PickrrZoneAdmin)
admin.site.register(PickrrServices)