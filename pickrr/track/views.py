from django.shortcuts import render_to_response,redirect, get_object_or_404, Http404
from django.template import RequestContext
from track.forms import TrackingForm
from track.models import Status
from orders.models import Order
from users.views import key_gen
from helpers.helpers import JsonResponse, correction_in_time

def search_tracking(request):
    trackingform = TrackingForm()
    if request.method=='POST':
        trackingform = TrackingForm(request.POST)
        if trackingform.is_valid():
            tracking_id = trackingform.cleaned_data['tracking_id']
            track_obj = get_object_or_404(Status,tracking_id=tracking_id)
            if not track_obj.key:
                track_obj.key = key_gen(track_obj.pk)
                track_obj.save()
            return redirect('track:show_tracking',track_obj.key,track_obj.pk)
    return render_to_response('track/search_tracking.html',
                            {'trackingform':trackingform},
                            context_instance=RequestContext(request))

def show_tracking(request,key,track_id):
    track_obj = get_object_or_404(Status,pk=track_id)
    if str(track_obj.key) != str(key):
        raise Http404
    return render_to_response('track/show_tracking.html',
                            {'track_obj':track_obj},
                            context_instance=RequestContext(request))

def tracking_api(request,tracking_id):
    track_obj = get_object_or_404(Status,tracking_id=str(tracking_id))
    response_data = {}
    response_data['order_status'] = 'Placed on ' + correction_in_time(track_obj.order.placed_at)
    if track_obj.pickup_time:
        response_data['pickup_status'] = 'Picked on ' + correction_in_time(track_obj.pickup_time)
    if track_obj.warehouse_receive_time:
        response_data['warehouse_status'] = 'Reached Pickrr Warehouse on ' + correction_in_time(track_obj.warehouse_receive_time)
    if track_obj.in_transit_time:
        if track_obj.in_transit_status:
            response_data['transit_status'] = str(track_obj.in_transit_status) + ' from ' + correction_in_time(track_obj.in_transit_time)
        else:
            response_data['transit_status'] = 'In Transit from ' + correction_in_time(track_obj.in_transit_time)
    if track_obj.out_delivery_time:
        response_data['delivery_status'] = 'Out for Delivery from ' + correction_in_time(track_obj.out_delivery_time)
    if track_obj.delivery_date:
        response_data['final_status'] = 'Successfully Delivered on ' + correction_in_time(track_obj.delivery_date)
    return JsonResponse(response_data)