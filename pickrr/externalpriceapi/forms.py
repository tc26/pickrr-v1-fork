from django import forms
from locations.models import City

class PriceApiForm(forms.Form):
    from_location = forms.ChoiceField(label="From Location",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    to_location = forms.ChoiceField(label="To Location",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    length = forms.FloatField(label='Length(in cms)',required=True)
    breadth = forms.FloatField(label='Breadth(in cms)',required=True)
    height = forms.FloatField(label='Height(in cms)',required=True)
    weight = forms.FloatField(label='Weight(in Kgs)',required=True)

    def __init__(self, *args, **kwargs):
        super(PriceApiForm, self).__init__(*args, **kwargs)
        #For dynamically changing the from city values as per our expansion plans
        self.fields['from_location'].choices = [ (from_city.id, str(from_city.name)) for from_city in City.objects.filter(from_service=True)]
        #For dynamically changing the to city
        self.fields['to_location'].choices = [ (to_city.id, str(to_city.name)) for to_city in City.objects.filter(to_service=True)]