from django.contrib import admin
from users.models import *

class MainUserAdmin(admin.ModelAdmin):
    list_display=('name','email','get_last_login','phone_number')

    search_fields=('name','email','phone_number')

    def get_last_login(self,obj):
        if obj.user and obj.user.last_login:
            return obj.user.last_login
        else:
            return 'undefined'

admin.site.register(MainUser,MainUserAdmin)
admin.site.register(PickupAddress)
admin.site.register(DeliveryAddress)