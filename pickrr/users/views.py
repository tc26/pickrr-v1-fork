from django.shortcuts import render_to_response, get_object_or_404, redirect, get_list_or_404, Http404
from django.core.urlresolvers import reverse
from django.template import Context, RequestContext
from django.core.context_processors import csrf
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from users.models import MainUser, PickupAddress, DeliveryAddress
from users.forms import SignupForm, LoginForm, PickupAddressForm, DeliveryAddressForm, ChangePasswordForm, ProfileEditForm, ForgotPasswordForm, SetPasswordForm
from orders.models import Order
#from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
import random
import string
from datetime import datetime, timedelta
import math
import hashlib
import json
from django.utils import timezone
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from locations.models import City
from helpers.helpers import read_post_json, JsonResponse, send_sms, clean_phone_number, generate_otp, generate_mobile_api_token, JsonQueryset
from django.views.decorators.csrf import csrf_exempt

User = get_user_model()

def key_gen(key):
    myvar = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits + string.ascii_uppercase) for _ in range(8))
    myvar += str(key)
    ckey = hashlib.md5(myvar).hexdigest()
    if not ckey:
        return key
    else:
        return ckey

#Signup View
def user_signup(request):
    signupform = SignupForm()
    if request.method=='POST':
        signupform = SignupForm(request.POST)
        if signupform.is_valid():
            password = signupform.cleaned_data['password']
            signup = signupform.save(commit=False)
            signup.save()
            #Max length of auth user is 30
            username = 'mainuser' + str(signup.pk)
            #creating a user object corresponding to the customer user
            user = User.objects.create_user(username,signup.email,password)
            user.is_active = True
            user.save()
            signup.user = user
            signup.save()
            #No verification email for now
            #Authenticate the user and allow login directly
            main_user = authenticate(username=username, password=password)
            if main_user is not None:
                if main_user.is_active:
                    login(request,main_user)
                    #successful login
                    if request.session.get('service_id',False):
                        request.session['user_id'] = signup.pk
                        return redirect('orders:checkout_pickup_address')
                    return HttpResponse(json.dumps({
                        "login": True
                        }), content_type="application/json")
                else:
                    return HttpResponse('Your account has been deactivated. Please contact 09818197991')
            else:
                #invalid email or password
                return HttpResponse('invalid email or password')
            #return redirect('users:user_account')
    return render_to_response('users/_user_signup.html',
                            {'signupform':signupform},
                            context_instance=RequestContext(request))

#Login View
def user_login(request):
    loginform = LoginForm()
    if request.method=='POST':
        loginform = LoginForm(request.POST)
        if loginform.is_valid():
            email = loginform.cleaned_data['email']
            password = loginform.cleaned_data['password']
            user_obj = get_object_or_404(MainUser,email=email,is_guest=False)
            main_user = authenticate(username=user_obj.user.username, password=password)
            if main_user is not None:
                if main_user.is_active:
                    login(request,main_user)
                    if request.session.get('service_id',False):
                        request.session['user_id'] = user_obj.pk
                        return redirect('orders:checkout_pickup_address')
                    #successful login
                    return HttpResponse(json.dumps({
                        "login": True
                        }), content_type="application/json")
                else:
                    return HttpResponse('Your account has been deactivated. Please contact 09818197991')
            else:
                #invalid email or password
                return HttpResponse('invalid email or password')
    return render_to_response('users/_user_login.html',
                            {'loginform':loginform},
                            context_instance=RequestContext(request))

def forgot_password(request):
    if request.user.is_authenticated():
        return redirect('users:user_account')
    forgot_password_form = ForgotPasswordForm()
    if request.method=='POST':
        forgot_password_form = ForgotPasswordForm(request.POST)
        if forgot_password_form.is_valid():
            email = forgot_password_form.cleaned_data['email']
            user_obj = get_object_or_404(MainUser,email=email,is_guest=False)
            if not user_obj.key:
                user_obj.key = key_gen(user_obj.pk)
                user_obj.save()
            set_link = 'http://www.pickrr.com/users/' + str(user_obj.key) + '/' + str(user_obj.pk) + '/set-password/'
            subject = "Set Password Link for " + str(email)
            body = render_to_string('users/set_password_email.html', {'user_obj':user_obj,'set_link':set_link})
            email = EmailMessage(subject, body, to=[user_obj.email])
            email.content_subtype = "html"
            email.send()
            return redirect('users:forgot_password_link_info')
    return render_to_response('users/forgot_password.html',
                            {'forgot_password_form':forgot_password_form},
                            context_instance=RequestContext(request))

def forgot_password_link_info(request):
    return render_to_response('users/forgot_password_link_info.html',
                            context_instance=RequestContext(request))

def set_password(request,key,user_id):
    if request.user.is_authenticated():
        return redirect('users:user_account')
    user = get_object_or_404(MainUser,pk=user_id)
    if str(user.key) != str(key):
        raise Http404
    set_password_form = SetPasswordForm(user_id=user_id)
    if request.method=='POST':
        set_password_form = SetPasswordForm(request.POST,user_id=user_id)
        if set_password_form.is_valid():
            return redirect('pickup:home_pickrr')
    return render_to_response('users/set_password.html',
                            {'set_password_form':set_password_form},
                            context_instance=RequestContext(request))

#User Account
@login_required(login_url='/users/login/')
def user_account(request):
    user_obj = get_object_or_404(MainUser,user=request.user)
    pickup_list = user_obj.pickupaddress_set.all()
    drop_list = user_obj.deliveryaddress_set.all()
    orders = Order.objects.filter(user=user_obj)
    return render_to_response('users/user_account.html',
                            {'pickup_list': pickup_list,
                            'drop_list': drop_list,
                            'orders': orders},
                            context_instance=RequestContext(request))

#User Account Edit view
@login_required(login_url='/users/login/')
def user_account_edit(request):
    user_obj = get_object_or_404(MainUser,user=request.user)
    account_edit_form = ProfileEditForm(instance=user_obj)
    if request.method=='POST':
        account_edit_form = ProfileEditForm(request.POST,instance=user_obj)
        if account_edit_form.is_valid():
            account_edit_form.save()
            return redirect('users:user_account')
    return render_to_response('users/user_account_edit.html',
                            {'account_edit_form':account_edit_form},
                            context_instance=RequestContext(request))

#User Account pickup bookings
@login_required(login_url='/users/login/')
def pickup_bookings(request):
    user_obj = get_object_or_404(MainUser,user=request.user)
    bookings = user_obj.order_set.all()
    return render_to_response('users/pickup_bookings.html',
                            {'bookings':bookings},
                            context_instance=RequestContext(request))

#Logout View
@login_required(login_url='/users/login')
def user_logout(request):
    logout(request)
    return redirect('pickup:home_pickrr')

#change password view
@login_required(login_url='/users/login')
def user_change_password(request):
    change_password_form = ChangePasswordForm(request=request)
    if request.method=='POST':
        change_password_form = ChangePasswordForm(request.POST,request=request)
        if change_password_form.is_valid():
            return redirect('users:user_account')
    return render_to_response('users/user_change_password.html',
                            {'change_password_form':change_password_form},
                            context_instance=RequestContext(request))

#Add address View
@login_required(login_url='/users/login')
def view_add_address(request):
    user_obj = get_object_or_404(MainUser,user=request.user)
    pickup_list = user_obj.pickupaddress_set.all()
    drop_list = user_obj.deliveryaddress_set.all()
    pickupform = PickupAddressForm()
    deliveryform = DeliveryAddressForm()
    if 'pickup_form' in request.POST:
        pickupform = PickupAddressForm(request.POST)
        if pickupform.is_valid():
            pickup_info = pickupform.save(commit=False)
            pickup_info.user=user_obj
            from_city = pickupform.cleaned_data['from_city']
            #from_state = pickupform.cleaned_data['from_state']
            pickup_info.city = dict(pickupform.fields['from_city'].choices)[int(from_city)]
            pickup_info.state = get_object_or_404(City,pk=int(from_city)).state.name
            pickup_info.save()
            return redirect('users:view_add_address')
    if 'delivery_form' in request.POST:
        deliveryform = DeliveryAddressForm(request.POST)
        if deliveryform.is_valid():
            delivery_info = deliveryform.save(commit=False)
            delivery_info.user=user_obj
            to_city = deliveryform.cleaned_data['to_city']
            #to_state = deliveryform.cleaned_data['to_state']
            delivery_info.city = dict(deliveryform.fields['to_city'].choices)[int(to_city)]
            delivery_info.state = get_object_or_404(City,pk=int(to_city)).state.name
            delivery_info.save()
            return redirect('users:view_add_address')
    return render_to_response('users/view_add_address.html',
                            {'pickupform':pickupform,
                            'deliveryform':deliveryform,
                            'pickup_list':pickup_list,
                            'drop_list':drop_list},
                            context_instance=RequestContext(request))

@csrf_exempt
def mobile_auth_api(request):
    if request.POST:
        #print "hello"
        login_data = read_post_json(request.body)
        email = login_data['email']
        phone_number = login_data['phone_number']
        new_user,old_user = MainUser.objects.get_or_create(email=email,is_guest=False)
        new_user.from_mobile = True
        new_user.phone_number = phone_number
        #new_user.mobile_token = generate_mobile_api_token(new_user.pk)
        new_user.otp = generate_otp(4)
        new_user.save()
        destination_number = clean_phone_number(str(phone_number))
        #print destination_number
        first_line = 'Dear customer' + '\n' 
        second_line = ('Your OTP for verification of mobile number',
                        str(phone_number),
                        'is',
                        str(new_user.otp))
        second_line = ' '.join(second_line)
        second_line = second_line + '.'
        message = first_line + second_line
        send_sms(destination_number,message)
        response_data = {}
        response_data['user_id'] = new_user.pk
        if new_user.name:
            response_data['name'] = new_user.name
        else:
            response_data['name'] = ''
        return JsonResponse(response_data)
    else:
        raise Http404

@csrf_exempt
def mobile_auth_api_otp(request):
    if request.POST:
        otp_data = read_post_json(request.body)
        user_id = otp_data['user_id']
        otp = otp_data['otp']
        new_user = get_object_or_404(MainUser,pk=int(user_id))
        if str(otp) != str(new_user.otp):
            message = 'Please enter a valid otp received on %s'%new_user.phone_number
            app_auth_token = ''
        else:
            message = 'success'
            app_auth_token = str(generate_mobile_api_token(new_user.pk))+ str(new_user.pk)
            new_user.mobile_token = app_auth_token
            new_user.save()
        response_data = {}
        response_data['message'] = message
        response_data['app_auth_token'] = app_auth_token
        return JsonResponse(response_data)
    else:
        raise Http404

def mobile_auth_api_otp_resend(request,user_id):
    new_user = get_object_or_404(MainUser,pk=int(user_id))
    new_user.otp = generate_otp(4)
    new_user.save()
    destination_number = clean_phone_number(str(new_user.phone_number))
    first_line = 'Dear customer' + '\n' 
    second_line = ('Your OTP for verification of mobile number',
                    str(new_user.phone_number),
                    'is',
                    str(new_user.otp))
    second_line = ' '.join(second_line)
    second_line = second_line + '.'
    message = first_line + second_line
    send_sms(destination_number,message)
    response_data = {}
    response_data['message'] = 'OTP sent'
    return JsonResponse(response_data)

def all_pickup_address_api(request,user_id,auth_token):
    user_obj = get_object_or_404(MainUser,pk=int(user_id))
    if str(user_obj.mobile_token) != str(auth_token):
        raise Http404
    all_pick_obj = user_obj.pickupaddress_set.all()
    return JsonQueryset(all_pick_obj)

def all_drop_address_api(request,user_id,auth_token):
    user_obj = get_object_or_404(MainUser,pk=int(user_id))
    if str(user_obj.mobile_token) != str(auth_token):
        raise Http404
    all_drop_obj = user_obj.deliveryaddress_set.all()
    return JsonQueryset(all_drop_obj)

@csrf_exempt
def mobile_edit_account(request,user_id,auth_token):
    if request.POST:
        #print "hello"
        user_obj = get_object_or_404(MainUser,pk=int(user_id))
        if str(user_obj.mobile_token) != str(auth_token):
            raise Http404
        account_data = read_post_json(request.body)
        name = account_data['full_name']
        email = account_data['email']
        phone_number = account_data['phone_number']
        is_change = False
        if name != '':
            user_obj.name = str(name)
            is_change = True
        if email != '':
            user_obj.email = str(email)
            is_change = True
        if phone_number != '':
            user_obj.phone_number = str(phone_number)
            is_change = True
        if is_change:
            user_obj.save()
        response_data = {}
        response_data['message'] = 'success'
        return JsonResponse(response_data)
    else:
        raise Http404