from django.db import models

class Order(models.Model):
    #order_id += order.pk
    order_id = models.CharField(max_length=256)
    user = models.ForeignKey('users.MainUser')
    pickup_address = models.ForeignKey('users.PickupAddress')
    drop_address = models.ForeignKey('users.DeliveryAddress')
    service_used = models.ForeignKey('pricingmodel.PickrrServices',blank=True,null=True)
    placed_at = models.DateTimeField(auto_now_add=True)
    scheduled_for = models.DateTimeField(blank=True,null=True)
    placed_at.editable = True
    total_bill = models.FloatField(default=0)
    invoice_sent = models.BooleanField(default=False)
    assigned_to = models.ForeignKey('pickrrs.Pickrrs',blank=True,null=True)
    is_cod = models.BooleanField(default=False)
    cod_amount = models.FloatField(default=0)
    is_cancelled = models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.order_id)

    class Meta:
        ordering = ['-placed_at']

class OrderItems(models.Model):
    order = models.ForeignKey('Order')
    product = models.ForeignKey('products.Product',blank=True,null=True)
    quantity = models.IntegerField(default=1)
    length = models.FloatField(default=0)
    breadth = models.FloatField(default=0)
    height = models.FloatField(default=0)
    weight = models.FloatField(default=0.5)
    item_bill = models.FloatField(default=0)

    def __unicode__(self):
        return self.order.user.email

    class Meta:
        ordering = ['-pk']